import flask
from flask import Flask
from flask_cors import CORS

from controllers.duplicate_controller import duplicate_controller
from controllers.node_controller import node_controller
from controllers.ping_controller import ping_controller
from controllers.resource_controller import resource_controller
from controllers.resources_controller import resources_controller
from controllers.state_controller import state_controller
from utils.init_database import init_database, init_queue_db
from utils.MessageAnnouncer import announcer

app = Flask(__name__)
CORS(app)

app.register_blueprint(node_controller, url_prefix='/api')
app.register_blueprint(resource_controller, url_prefix='/api')
app.register_blueprint(resources_controller, url_prefix='/api')
app.register_blueprint(state_controller, url_prefix='/api')
app.register_blueprint(duplicate_controller, url_prefix='/api')
app.register_blueprint(ping_controller, url_prefix='/api')


@app.route('/api/subscribe', methods=['GET'])
def subscribe():
    def gen():
        q = announcer.listen()
        while True:
            # wait for the next message to be sent
            result = q.get()
            # stream the message back to the client
            yield result

    return flask.Response(gen(), mimetype='text/event-stream')


if __name__ == '__main__':
    init_database()
    init_queue_db()
    app.run(host="0.0.0.0", port=3000, debug=True, use_reloader=True)
