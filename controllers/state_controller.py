import time

from flask import Blueprint, request
from flask_restful import Api, Resource

from consts.status import Status
from utils.database_utils import clear_command_queue, clear_resource_queue, get_command_queue, get_resource_queue, get_state, insert_command, insert_resource, update_state
from utils.http_utils import normalize_url

state_controller = Blueprint('state_controller', __name__)
api = Api(state_controller)


class StopNode(Resource):
    def put(self):
        if get_state() == Status.OFFLINE:
            return {"message": "Node already stoped."}, 403
        else:
            node_address = normalize_url(request.host_url)
            update_state(Status.OFFLINE, node_address)
            return {"message": "Successfully stoped."}, 200


class StartNode(Resource):
    def put(self):
        if get_state() == Status.ONLINE:
            return {"message": "Node already started."}, 403
        else:
            node_address = normalize_url(request.host_url)
            node_address_intern = normalize_url(request.host_url, True)
            update_state(Status.STARTING, node_address)

            resources = get_resource_queue(node_address_intern)
            for resource in resources:
                insert_resource(resource)
            clear_resource_queue(node_address_intern)

            commands = get_command_queue(node_address_intern)
            for command in commands:
                resource_id = command['resource_id']
                cm = command['command']
                insert_command(resource_id, cm)
            clear_command_queue(node_address_intern)

            time.sleep(5)
            update_state(Status.ONLINE, node_address)

            return {"message": "Successfully started."}, 200


api.add_resource(StopNode, '/stop')
api.add_resource(StartNode, '/start')
