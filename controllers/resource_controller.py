from typing import Optional

from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from models.command import Command
from utils.database_utils import (get_resource, get_state, insert_command,
                                  update_state)
from utils.http_utils import normalize_url, send_command_to_other_nodes

resource_controller = Blueprint('resource_controller', __name__)
api = Api(resource_controller)


class ResourceResource(Resource):
    def get(self, id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403

        resource = get_resource(id, True)
        if resource:
            return jsonify(resource.to_full_dict())
        else:
            return {"message": "Resource not found"}, 404

    def post(self, id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403
        node_address = normalize_url(request.host_url)
        update_state(Status.UPDATING, node_address)

        request_command: Optional[str] = request.get_json().get('command')
        request_addresses: list[str] = request.get_json().get('addresses', [])

        if request_command is None:
            update_state(Status.ONLINE, node_address, {"message": "command is missing", "statusCode": 400})
            return {"message": "command is missing"}, 400

        resource = get_resource(id)
        if resource is None:
            update_state(Status.ONLINE, node_address, {"message": "Resource not found", "statusCode": 404})
            return {"message": "Resource not found"}, 404

        request_addresses = list(map(lambda x: normalize_url(x, True), request_addresses))

        if len(request_addresses) > 0 and node_address in request_addresses:
            request_addresses.remove(node_address)

        command = Command(request_command)

        # TODO: Potentiellement long, à mettre dans un thread ?
        send_command_to_other_nodes(request_addresses, id, command)

        insert_command(id, command)
        update_state(Status.ONLINE, node_address)
        return jsonify(get_resource(id, True).to_full_dict())

    def delete(self, id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403
        node_address = normalize_url(request.host_url)
        update_state(Status.UPDATING, node_address)

        request_addresses: list[str] = request.get_json().get('addresses', [])
        request_addresses = list(map(lambda x: normalize_url(x, True), request_addresses))

        resource = get_resource(id)
        if resource is None:
            update_state(Status.ONLINE, node_address, {"message": "Resource not found", "statusCode": 404})
            return {"message": "Resource not found"}, 404

        if len(request_addresses) > 0 and node_address in request_addresses:
            request_addresses.remove(node_address)

        command = Command("DELETE")

        # TODO: Potentiellement long, à mettre dans un thread ?
        send_command_to_other_nodes(request_addresses, id, command)

        insert_command(id, command)
        update_state(Status.ONLINE, node_address)
        return jsonify(get_resource(id, True).to_full_dict())


api.add_resource(ResourceResource, '/resource/<string:id>')
