from typing import Optional

from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from models.command import Command
from models.resource import Resource as ResourceModel
from utils.database_utils import (get_resource, get_state, insert_command,
                                  insert_resource, update_state)
from utils.http_utils import normalize_url

duplicate_controller = Blueprint('duplicate_controller', __name__)
api = Api(duplicate_controller)


class DuplicateResourceResource(Resource):
    def put(self):
        state = get_state()
        if state == Status.OFFLINE:
            return response_offline, 403

        update_state(Status.UPDATING, normalize_url(request.host_url))

        body = request.get_json()
        resource = body.get('resource')

        if resource is None:
            update_state(Status.ONLINE, normalize_url(request.host_url), {"message": "Resource is missing from body", "statusCode": 400})
            return {"message": "Resource is missing from body"}, 400

        resource = ResourceModel.from_full_dict(resource)

        if get_resource(resource.id) is not None:
            update_state(Status.ONLINE, normalize_url(request.host_url), {"message": "Resource already exists", "statusCode": 400})
            return {"message": "Resource already exists"}, 400

        insert_resource(resource)
        insert_command(resource.id, resource.commands[0])
        update_state(Status.ONLINE, normalize_url(request.host_url))

        return resource.to_full_dict(), 201


class DuplicateCommandResource(Resource):
    def put(self, id: str):
        state = get_state()
        if state == Status.OFFLINE:
            return response_offline, 403

        update_state(Status.UPDATING, normalize_url(request.host_url))

        resource = get_resource(id)
        if resource is None:
            update_state(Status.ONLINE, normalize_url(request.host_url), {"message": "Resource not found", "statusCode": 404})
            return {"message": "Resource not found"}, 404

        request_command: Optional[str] = request.get_json().get('command')
        if request_command is None:
            update_state(Status.ONLINE, normalize_url(request.host_url), {"message": "command is missing", "statusCode": 400})
            return {"message": "command is missing"}, 400

        command = Command.from_full_dict(request_command)

        insert_command(id, command)
        update_state(Status.ONLINE, normalize_url(request.host_url))

        return command.to_dict(), 201


api.add_resource(DuplicateResourceResource, '/duplicate/resource')
api.add_resource(DuplicateCommandResource,
                 '/duplicate/resource/<string:id>/command')
