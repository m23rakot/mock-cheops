import os

from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from utils.database_utils import count_resources, get_state
from utils.http_utils import normalize_url

node_controller = Blueprint('node_controller', __name__)
api = Api(node_controller)
node_address = os.environ.get('NODE_FILE')


class NodeResource(Resource):
    def get(self):
        state = get_state()
        if state == Status.OFFLINE:
            return response_offline, 403

        count = count_resources()
        return jsonify({
            "state": state.value,
            "resourcesCount": count if count != -1 else 0,
            "address": normalize_url(request.host_url)
        })


api.add_resource(NodeResource, '/node')
