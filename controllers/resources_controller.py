from typing import Optional

from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from models.command import Command
from models.resource import Resource as ResourceModel
from utils.database_utils import (get_resources, get_state, insert_command,
                                  insert_resource, update_state)
from utils.http_utils import normalize_url, send_resource_to_other_nodes

resources_controller = Blueprint('resources_controller', __name__)
api = Api(resources_controller)


class ResourcesResource(Resource):
    def get(self):
        if get_state() == Status.OFFLINE:
            return response_offline, 403

        resources = get_resources(
            page=request.headers.get('page', 1, type=int),
            page_size=request.headers.get('pageSize', 20, type=int))
        
        return jsonify([resource.to_simple_dict() for resource in resources])

    def put(self):
        if get_state() == Status.OFFLINE:
            return response_offline, 403
        node_address = normalize_url(request.host_url)
        update_state(Status.UPDATING, node_address)

        request_command: Optional[str] = request.get_json().get('command')
        request_name: Optional[str] = request.get_json().get('name')
        request_addresses: list[str] = request.get_json().get('addresses', [])
        request_addresses = list(map(lambda x: normalize_url(x, True), request_addresses))

        if request_command is None:
            update_state(Status.ONLINE, node_address, {"message": "command is missing", "statusCode": 400})
            return {"message": "command is missing"}, 400

        if request_name is None:
            update_state(Status.ONLINE, node_address, {"message": "name is missing", "statusCode": 400})
            return {"message": "name is missing"}, 400

        if len(request_addresses) != 0 and node_address in request_addresses:
            request_addresses.remove(node_address)

        resource = ResourceModel(
            request_name,
            commands=[Command(request_command)]
        )

        # TODO: Potentiellement long, à mettre dans un thread ?
        send_resource_to_other_nodes(request_addresses, resource)

        insert_resource(resource)
        insert_command(resource.id, resource.commands[0])

        update_state(Status.ONLINE, node_address)

        return jsonify(resource.to_simple_dict())


api.add_resource(ResourcesResource, '/resources')
