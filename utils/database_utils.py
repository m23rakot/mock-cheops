import json
import os
import sqlite3
import sys
from typing import Optional

from consts.status import Status
from models.command import Command
from models.resource import Resource
from utils.MessageAnnouncer import send_status_change

db_file = 'assets/' + os.environ.get('NODE_FILE') + '.db'
db_queue_file = 'assets/queue.db'


def update_state(state: Status, node_address: str = None, extra_data: dict = None) -> bool:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute("UPDATE state SET state = ?", (state.value,))
            db_conn.commit()
            cursor.close()
            if node_address is not None:
                send_status_change(node_address=node_address, status=state, extra_data=extra_data)
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False


def get_state() -> Status:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute("SELECT state FROM state")
            state = cursor.fetchone()
            cursor.close()
            return Status(state[0])
        except Exception as e:
            print(e, file=sys.stderr)
            return None


def insert_resource(resource: Resource) -> bool:
    with sqlite3.connect(db_file) as db_conn:
        try:
            tp = resource.to_tuple()
            cursor = db_conn.cursor()
            cursor.execute(
                "INSERT INTO resources (id, name, last_update) VALUES (?, ?, ?)", tp)
            db_conn.commit()
            cursor.close()
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False


def get_resources(with_commands: bool = False, page: int = 1, page_size=20) -> Optional[list[Resource]]:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute("SELECT resources.id, resources.name, resources.last_update FROM resources ORDER BY last_update DESC LIMIT ? OFFSET ?",
                           (page_size, (page - 1) * page_size))
            resources: list[tuple] = cursor.fetchall()
            cursor.close()
            if with_commands:
                return [Resource.from_tuple(resource, commands=get_commands(resource[0]), deleted=resource_id_is_deleted(resource[0])) for resource in resources]

            return [Resource.from_tuple(resource, commands_count=get_commands_count(resource[0]),deleted=resource_id_is_deleted(resource[0])) for resource in resources]
        except Exception as e:
            print(e, file=sys.stderr)
            return None


def count_resources() -> int:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute("SELECT COUNT(*) FROM resources")
            count = cursor.fetchone()
            cursor.close()
            return count[0]
        except Exception as e:
            print(e, file=sys.stderr)
            return -1


def get_resource(id: str, with_commands: bool = False) -> Optional[Resource]:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "SELECT resources.id, resources.name, resources.last_update FROM resources WHERE id = ?", (id,))
            resource = cursor.fetchone()
            cursor.close()
            if resource is None:
                return None
            
            deleted = resource_id_is_deleted(id)
            if with_commands:
                return Resource.from_tuple(resource, commands=get_commands(id), deleted=deleted)
            return Resource.from_tuple(resource, commands_count=get_commands_count(id), deleted=deleted)
        except Exception as e:
            print(e, file=sys.stderr)
            return None

def resource_id_is_deleted(resource_id: int) -> bool:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "SELECT command FROM commands WHERE resource_id = ? ORDER BY create_date DESC LIMIT 1", (resource_id,))
            command = cursor.fetchone()
            cursor.close()
            return command[0] == "DELETE"
        except Exception as e:
            print(e, file=sys.stderr)
            return True

def resource_is_deleted(resource: Resource) -> bool:
    return resource_id_is_deleted(resource.id)


def insert_command(resource_id: str, command: Command) -> bool:
    with sqlite3.connect(db_file) as db_conn:
        try:
            tp = command.to_tuple(resource_id)
            cursor = db_conn.cursor()
            cursor.execute(
                "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)", tp)
            # update resource last_update
            cursor.execute("UPDATE resources SET last_update = ? WHERE id = ?",
                           (command.date, resource_id))
            db_conn.commit()
            cursor.close()
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False


def get_commands(resource_id: str, page: int = 1, page_size=20) -> Optional[list[Command]]:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute("SELECT id, create_date, command FROM commands WHERE resource_id = ? ORDER BY create_date DESC LIMIT ? OFFSET ?",
                           (resource_id, page_size, (page - 1) * page_size))
            commands = cursor.fetchall()
            cursor.close()
            return [Command.from_tuple(command) for command in commands]
        except Exception as e:
            print(e, file=sys.stderr)
            return None


def get_commands_count(resource_id: str) -> int:
    with sqlite3.connect(db_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "SELECT COUNT(*) FROM commands WHERE resource_id = ?", (resource_id,))
            count = cursor.fetchone()
            cursor.close()
            return count[0]
        except Exception as e:
            print(e, file=sys.stderr)
            return -1


def get_command_queue(node_address: str) -> Optional[list]:
    with sqlite3.connect(db_queue_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "SELECT resource_id, command_object FROM command_queue WHERE node_address = ? ORDER BY id DESC", (node_address,))
            commands = cursor.fetchall()
            cursor.close()

            return [{
                "resource_id": command[0],
                "command": Command.from_full_dict(eval(command[1]))
            } for command in commands]
        except Exception as e:
            print(e, file=sys.stderr)
            return None


def get_resource_queue(node_address: str) -> Optional[list[Resource]]:
    with sqlite3.connect(db_queue_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "SELECT resource_object FROM resource_queue WHERE node_address = ? ORDER BY id DESC", (node_address,))
            resources_json: str = cursor.fetchall()
            cursor.close()

            resources: list[dict] = [eval(resource[0])
                                     for resource in resources_json]
            return [Resource.from_full_dict(resource) for resource in resources]
        except Exception as e:
            print(e, file=sys.stderr)
            return None


def insert_resource_queue(node_address: str, resource: Resource) -> bool:
    with sqlite3.connect(db_queue_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "INSERT INTO resource_queue (node_address, resource_object) VALUES (?, ?)", (node_address, json.dumps(resource.to_full_dict()),))
            db_conn.commit()
            cursor.close()
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False


def insert_command_queue(node_address: str, resource_id: str, command: Command) -> bool:
    with sqlite3.connect(db_queue_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute("INSERT INTO command_queue (node_address, resource_id, command_object) VALUES (?, ?, ?)",
                           (node_address, resource_id, json.dumps(command.to_dict()))),
            db_conn.commit()
            cursor.close()
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False


def clear_resource_queue(node_address: str) -> bool:
    with sqlite3.connect(db_queue_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "DELETE FROM resource_queue WHERE node_address = ?", (node_address,))
            db_conn.commit()
            cursor.close()
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False


def clear_command_queue(node_address: str) -> bool:
    with sqlite3.connect(db_queue_file) as db_conn:
        try:
            cursor = db_conn.cursor()
            cursor.execute(
                "DELETE FROM command_queue WHERE node_address = ?", (node_address,))
            db_conn.commit()
            cursor.close()
            return True
        except Exception as e:
            print(e, file=sys.stderr)
            return False
