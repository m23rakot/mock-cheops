import random


def generate_hexadecimal(length: int) -> str:
    hex_characters = '0123456789abcdef'
    hex_string = ''.join(random.choice(hex_characters) for _ in range(length))
    return hex_string
