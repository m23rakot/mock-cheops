import json
import sys
from urllib.parse import urlparse

import requests

from models.command import Command
from models.resource import Resource
from utils.database_utils import insert_command_queue, insert_resource_queue

nodes = {
    'http://localhost:3000/': 'http://node1:3000/',
    'http://localhost:3001/': 'http://node2:3000/',
    'http://localhost:3002/': 'http://node3:3000/',
    'http://localhost:3003/': 'http://node4:3000/',
}


def normalize_url(url: str, inter: bool = False):
    parsed_url = urlparse(url)
    format = parsed_url.scheme + "://" + parsed_url.netloc + "/"
    if inter:
        url = nodes.get(url,None)
        if url is None:
            print('normalize intern', url, file=sys.stderr)
            return
        return url
    else:
        return format


def send_resource_to_other_nodes(addresses: list[str], resource: Resource):
    for address in addresses:
        try:
            result = requests.put(
                f"{address}/api/duplicate/resource",
                data=json.dumps({
                    "resource": resource.to_full_dict(),
                }),
                headers={"Content-Type": "application/json"})
            if result.status_code != 201:
                print('Address', address, 'Status code:',
                      result.status_code, file=sys.stderr)
                insert_resource_queue(address, resource)
        except requests.exceptions.RequestException as e:
            print('Address', address, e, file=sys.stderr)
            insert_resource_queue(address, resource)
        except Exception as e:
            print('Address', address, e, file=sys.stderr)
            insert_resource_queue(address, resource)


def send_command_to_other_nodes(addresses: list[str], resource_id: str, command: Command):
    for address in addresses:
        try:
            result = requests.put(
                f"{address}/api/duplicate/resource/{resource_id}/command",
                data=json.dumps({
                    "command": command.to_dict(),
                }),
                headers={"Content-Type": "application/json"})
            if result.status_code != 201:
                print('Address', address, 'Status code:',
                      result.status_code, file=sys.stderr)
                insert_command_queue(address, resource_id, command)
        except requests.exceptions.RequestException as e:
            print('Address', address, e, file=sys.stderr)
            insert_command_queue(address, resource_id, command)
        except Exception as e:
            print('Address', address, e, file=sys.stderr)
            insert_command_queue(address, resource_id, command)

def ping(addresses: list[str]):
    results = {}
    for address in addresses:
        try:
            result = requests.get(f"{address}/api/ping")
            if result.status_code != 200:
                print('Address', address, 'Status code:',
                      result.status_code, file=sys.stderr)
                results[address] = result.status_code == 200
        except requests.exceptions.RequestException as e:
            print('Address', address, e, file=sys.stderr)
            results[address] = False
        except Exception as e:
            print('Address', address, e, file=sys.stderr)
            results[address] = False
    return results