import os
import sqlite3

from consts.status import Status

from .database_utils import db_file, db_queue_file

status = {
    "node1": {"state": Status.ONLINE.value},
    "node2": {"state": Status.ONLINE.value},
    "node3": {"state": Status.ONLINE.value},
    "node4": {"state": Status.ONLINE.value},
}

resources = [
    ("746f746f2e747874a", 'Resource 1', 1699272000000),
    ("e539b187571ec476a", 'Resource 2', 1699279200000),
    ("3ad06e1ea9a9c2085", 'Resource 3', 1699272180000),
    ("0eaf0d20311e24abd", 'Resource 4', 1699358580000),
    ("183ca5d3a2f6735bf", 'Resource 5', 1699359580000),
    ("640b0507b542f5854", 'Resource 6', 1699657890000),
]

commands = {
    "746f746f2e747874a": [
        ("746f746f2e747874a", "7363616c616c61", 1699279200000, "mkdir /tmp/test"),
        ("746f746f2e747874a", "7363616c616c62",
         1699279300000, "touch /tmp/test/test.txt"),
        ("746f746f2e747874a", "7363616c616c63", 1699279400000, "rm -rf /tmp/test"),
    ],
    "e539b187571ec476a": [
        ("e539b187571ec476a", "7363616c616c64", 1699279287000, "mkdir /tmp/test2"),
        ("e539b187571ec476a", "7363616c616c65",
         1699279373000, "touch /tmp/test2/test.txt"),
        ("e539b187571ec476a", "7363616c616c66",
         1699279428000, "rm -rf /tmp/test2"),
    ],
    "3ad06e1ea9a9c2085": [
        ("3ad06e1ea9a9c2085", "7363616c616c67", 1699272280000, "mkdir /tmp/test3"),
        ("3ad06e1ea9a9c2085", "7363616c616c68",
         1699272380000, "touch /tmp/test3/test.txt"),
        ("3ad06e1ea9a9c2085", "7363616c616c69",
         1699272480000, "rm -rf /tmp/test3"),
    ],
    "0eaf0d20311e24abd": [
        ("0eaf0d20311e24abd", "7363616c616c7a", 1699358580000, "mkdir /tmp/test4"),
        ("0eaf0d20311e24abd", "7363616c616c7b",
         1699358680000, "touch /tmp/test4/test.txt"),
        ("0eaf0d20311e24abd", "7363616c616c7c",
         1699358780000, "rm -rf /tmp/test4"),
    ],
    "183ca5d3a2f6735bf": [
        ("183ca5d3a2f6735bf", "7363616c616c7d", 1699359580000, "mkdir /tmp/test5"),
        ("183ca5d3a2f6735bf", "7363616c616c7e",
         1699360580000, "touch /tmp/test5/test.txt"),
        ("183ca5d3a2f6735bf", "7363616c616c7f",
         1699361580000, "rm -rf /tmp/test5"),
    ],
    "640b0507b542f5854": [
        ("640b0507b542f5854", "7363616c616c80", 1699657890000, "mkdir /tmp/test6"),
        ("640b0507b542f5854", "7363616c616c81",
         1699658890000, "touch /tmp/test6/test.txt"),
        ("640b0507b542f5854", "7363616c616c82",
         1699659890000, "rm -rf /tmp/test6"),
    ],
}


def init_database():
    with sqlite3.connect(db_file) as db_conn:
        current_node = os.environ.get('NODE_FILE')
        # check if tables "state", "resources" and "commands" exist
        cursor = db_conn.cursor()
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND (name='state' OR name='resources' OR name='commands')")
        tables = cursor.fetchall()
        cursor.close()

        if len(tables) == 3:
            print("Database already initialized")
            return

        # create tables
        cursor = db_conn.cursor()
        # state (id (autoincrement), state)
        print("Creating table state")
        cursor.execute(
            "CREATE TABLE state (id INTEGER PRIMARY KEY AUTOINCREMENT, state TEXT)")
        # resources (id, name, last_update)
        print("Creating table resources")
        cursor.execute(
            "CREATE TABLE resources (id TEXT PRIMARY KEY, name TEXT, last_update INTEGER)")
        # commands (resource_id, id, create_date, command)
        print("Creating table commands")
        cursor.execute("CREATE TABLE commands (resource_id TEXT, id TEXT, create_date INTEGER, command TEXT, PRIMARY KEY (resource_id, id), FOREIGN KEY (resource_id) REFERENCES resources(id))")

        # insert data
        if current_node == 'node1':
            insert_node1(cursor, db_conn)
        elif current_node == 'node2':
            insert_node2(cursor, db_conn)
        elif current_node == 'node3':
            insert_node3(cursor, db_conn)
        elif current_node == 'node4':
            insert_node4(cursor, db_conn)

        cursor.close()


def init_queue_db():
    with sqlite3.connect(db_queue_file) as db_conn:
        # check if table "queue" exists
        cursor = db_conn.cursor()
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='resource_queue' OR name='command_queue'")
        tables = cursor.fetchall()
        cursor.close()

        if len(tables) == 2:
            print("Queue database already initialized")
            return

        # create table
        cursor = db_conn.cursor()
        # queue (id (autoincrement), resource_id, command, date, status)
        print("Creating table resource_queue")
        cursor.execute(
            "CREATE TABLE resource_queue (id INTEGER PRIMARY KEY AUTOINCREMENT, node_address TEXT, resource_object TEXT)")
        print("Creating table command_queue")
        cursor.execute(
            "CREATE TABLE command_queue (id INTEGER PRIMARY KEY AUTOINCREMENT, node_address TEXT, resource_id TEXT, command_object TEXT)")
        db_conn.commit()


def insert_node1(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node1")
    cursor.execute("INSERT INTO state (state) VALUES (?)",
                   (status["node1"]["state"],))
    cursor.executemany(
        "INSERT INTO resources (id, name, last_update) VALUES (?, ?, ?)", resources)
    for _, commands_list in commands.items():
        cursor.executemany(
            "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)", commands_list)
    db_conn.commit()


def insert_node2(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node2")
    cursor.execute("INSERT INTO state (state) VALUES (?)",
                   (status["node2"]["state"],))
    rs = resources[1:3]
    cursor.executemany(
        "INSERT INTO resources (id, name, last_update) VALUES (?, ?, ?)", rs)
    for r in rs:
        cursor.executemany(
            "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)", commands[r[0]])
    db_conn.commit()


def insert_node3(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node3")
    cursor.execute("INSERT INTO state (state) VALUES (?)",
                   (status["node3"]["state"],))
    rs = resources[3:5]
    cursor.executemany(
        "INSERT INTO resources (id, name, last_update) VALUES (?, ?, ?)", rs)

    cs = commands[rs[0][0]]
    cs.append(("0eaf0d20311e24abd", "7363616c616c8a",
              1699362580000, "ls -l /tmp"))
    cursor.executemany(
        "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)", cs)

    cs = commands[rs[1][0]]
    cs.pop()
    cursor.executemany(
        "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)", cs)
    db_conn.commit()


def insert_node4(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node4")
    cursor.execute("INSERT INTO state (state) VALUES (?)",
                   (status["node4"]["state"],))
    rs = resources[5:]
    cursor.executemany(
        "INSERT INTO resources (id, name, last_update) VALUES (?, ?, ?)", rs)
    for r in rs:
        cursor.executemany(
            "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)", commands[r[0]])
    db_conn.commit()
